REPOSITORIO MAVEN MITECMOBILE
=============================


Uso en aplicaciones de una librería desplegadas
-----------------------------------------------

### Maven público. Añadir la siguiente linea configuración en el archivo build.gradle del proyecto

```
allprojects {
    repositories {
        jcenter()
        maven {            
            url "https://api.bitbucket.org/1.0/repositories/development_mitecmobile/maven_repository/raw/releases"
        }
    }
}
```

### Incluir la siguiente dependencia en el archivo de build.gradle de la aplicación:
```
dependencies {
    ...
    compile '<nombre_paquete_libreria>:<nombre_proyecto_libreria>:<version_libreria>'
    ...
}
```